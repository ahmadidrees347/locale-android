package com.oranjeclick.practiceapp.locale;


import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.ArrayList;

public class AppPreference {
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Context context;

    // Constructor
    public AppPreference(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences("com.system", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public void setLanguage(String language) {
        editor.putString("language_key", language);
        editor.apply();
        editor.commit();
    }

    public String getLanguage() {
        return sharedPreferences.getString("language_key", "en");
    }

}