package com.oranjeclick.practiceapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.oranjeclick.practiceapp.locale.AppPreference;
import com.oranjeclick.practiceapp.locale.LocaleHelper;
import com.oranjeclick.practiceapp.locale.LocaleManager;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    Button btn1, btn2, btn3;
    AppPreference appPreference;
    TextView txt;
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        appPreference = new AppPreference(getApplicationContext());
        btn1 = findViewById(R.id.btn1);
        btn2 = findViewById(R.id.btn2);
        btn3 = findViewById(R.id.btn3);
        txt = findViewById(R.id.txt);
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setLocale("fr");
                txt.setText(getResources().getString(R.string.title));

                setLanguagePref(MainActivity.this, "fr");
            }
        });
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setLocale("en");
                /*if(LocaleManager.getLanguagePref(MainActivity.this).equalsIgnoreCase("en")){
                    //setNewLocale(MainActivity.this, LocaleManager.ARABIC);
                    //LocaleHelper.onAttach(MainActivity.this, "ar");
                    setLocale("ar");
                }
                else {
                    //LocaleHelper.onAttach(MainActivity.this, "en");
                    //setNewLocale(MainActivity.this, LocaleManager.ENGLISH);
                    setLocale("fr");
                }*/
                txt.setText(getResources().getString(R.string.title));
                setLanguagePref(MainActivity.this, "en");
            }
        });
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setLocale("ar");
                txt.setText(getResources().getString(R.string.title));

                setLanguagePref(MainActivity.this, "ar");
            }
        });
    }
    public void setLocale(String lang) {

        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        Locale.setDefault(myLocale);
        onConfigurationChanged(conf);
    }
    private void setNewLocale(AppCompatActivity mContext, @LocaleManager.LocaleDef String language) {
        /*Configuration newConfig1 = getApplicationContext().getResources().getConfiguration();
        newConfig1.setLocale(new Locale(language));
        onConfigurationChanged(newConfig1);

        LocaleManager.setNewLocale(this, language);*/
        /*Intent intent = mContext.getIntent();
        startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
        overridePendingTransition(0, 0);*/


        Locale myLocale = new Locale(language);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        onConfigurationChanged(conf);
    }
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        txt.setText(getResources().getString(R.string.title));
        super.onConfigurationChanged(newConfig);
        //LocaleManager.setLocale(this);
    }

    private static void setLanguagePref(Context mContext, String localeKey) {
        // SharedPreferences mPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        AppPreference appPreference = new AppPreference(mContext);
        appPreference.setLanguage(localeKey);
        //mPreferences.edit().putString(LANGUAGE_KEY, localeKey).apply();
    }
    public static String getLanguagePref(Context mContext) {
        //SharedPreferences mPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        AppPreference appPreference = new AppPreference(mContext);
        return appPreference.getLanguage();
    }

    @Override
    protected void onResume() {

        setLocale(getLanguagePref(getApplicationContext()));
        super.onResume();
    }
}
